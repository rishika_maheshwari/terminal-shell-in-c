The shell must support the following internal commands:
1.cd <directory> - Change the current default directory to
2.<directory>. If the <directory> argument is not present, report the
3.current directory. If the directory does not exist an appropriate error
4.should be reported. This command should also change the PWD
5.environment variable.
6.cls - Clear the screen.
7.dir <directory> - List the contents of directory <directory>.
8.copy <source> <destination> - Copy the <source> folder to
9.<destination>.
10.v. print <comment> - Display <comment> on the display followed by a new
11.line (multiple spaces/tabs may be reduced to a single space).
12.md <directory> - Create the folder <directory>.
13.rd <directory> - Remove the folder <directory> if the folder is
14.empty, should display error message if it is not.
15.quit - Quit the shell.

Description of the Code : 
(1)We assumed maximum length of a command can be of 256 characters(MAX_LINE), count denotes total number of commands executed until now and maximum no of arguments in a command can be 129 (MAX_LINE/2 + 1). 
(2)main(void) function initializes inputBuffer to store the current command and args[] array for parsing the current command into arguments.
(3)We use different size of buffers to execute he commands properly.
Some of the important Library functions used are as follows �C 
(a)For executing 
cd command :: chdir(directory)---Library/Header file(unistd.h)
char* getcwd(char* buffer,size_t size)---Library/Header file(unistd.h)
(b) For executing all types of rd commands :: First,file is opened using DIR* opendir(const char *name) function, then read using struct dirent readdir(DIR* dir) function and removed files/directories according to the given command using int remove(const char *filename) function and finally closed the file using int closedir(DIR* dir) function.Libraries/Header files (dirent.h,sys/types.h) are included for using the builtin library functions. 
(c) For Creating a child process to run and supporting the redirection operators > and < to redirect the input and ouput of the program to indicated files.


